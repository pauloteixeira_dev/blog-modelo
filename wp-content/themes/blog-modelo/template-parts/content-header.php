<?php
/**
 * Created by PhpStorm.
 * User: Paulo Teixeira
 * Date: 29/07/2018
 * Time: 16:03
 */
?>
<header id="navegacao">
   <div class="container encher-vertical">
       <div class="row encher-vertical">
           <div class="col-sm-12 col-12 d-flex align-items-center">
               <nav class="list-horizontal">
                   <?php wp_nav_menu(['theme_location' => 'menu-principal']); ?>
               </nav>
           </div>
       </div>
   </div>
</header>
