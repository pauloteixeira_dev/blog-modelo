<?php
/**
 * Created by PhpStorm.
 * User: Paulo Teixeira
 * Date: 26/07/2018
 * Time: 21:54
 * >>>>>>>>>>>> O modelo principal. Se o seu Tema fornecer seus próprios modelos, index.php deverá estar presente.
 */
?>
<?php get_header(); ?>
<main>
    <section class="list-post">
        <div class="container">
            <div class="row">

                <?php
                $posts = query_postagens( ['posts_per_page'=>10,  'paged' => pagina_atual()]);
                    if($posts->have_posts()){
                        while ( $posts->have_posts()){ $posts->the_post()?>
                        <div class="col-sm-12">
                            <span>Postagem</span>
                        </div>
                    <?php }
                    }else{ ?>
                        <span>nenhum registro</span>
                    <?php } ?>
                <?php paginacao($posts); ?>

            </div>
        </div>
    </section>
</main>
<?php get_footer(); ?>

