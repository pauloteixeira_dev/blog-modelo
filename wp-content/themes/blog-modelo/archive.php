<?php
/**
 * Created by PhpStorm.
 * User: Paulo Teixeira
 * Date: 26/07/2018
 * Time: 21:54
 * >>>>>>>>>>>> O modelo de arquivo. Usado quando uma categoria, autor ou data é consultada.
 * Observe que esse modelo será substituído por category.php , author.php e date.php para os respectivos tipos de consulta.
 */