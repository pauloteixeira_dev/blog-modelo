<?php
/**
 * Created by PhpStorm.
 * User: Paulo Teixeira
 * Date: 26/07/2018
 * Time: 21:49
 * >>>>>>>>>>>>
 */

// VARIAVEIS GLOBAIS
define( 'ROOT', get_template_directory_uri());

//ADD SUPORTE DE LOGO
add_theme_support('custom-logo');

//ADICIONAR IMAGENS DESTACADAS
add_theme_support('post-thumbnails');

//ADD VENDORS
function add_vendor(){
    //bootstrap
    wp_enqueue_style( 'bootstrap-style', ROOT . '/vendor/bootstrap-4.1.2/css/bootstrap.min.css');
    //icomoon
    wp_enqueue_style( 'icomoom', ROOT . '/vendor/bootstrap-4.1.2/css/bootstrap.min.css' );
    //jquery
    wp_enqueue_script( 'js-jquery', ROOT . '/vendor/jquery/jquery-3.3.1.min.js', array(), false, true);
    //owl-carousel
    wp_enqueue_script('owl-carousel', ROOT . '/vendor/owl-carousel2-2.3.4/dist/owl.carousel.js', array(), false, true);
    wp_enqueue_style('owl-carousel-css', ROOT . '/vendor/owl-carousel2-2.3.4/dist/assets/owl.carousel.min.css');
    //slider-tx
    wp_enqueue_style('owl-carousel-css', ROOT . '/vendor/owl-carousel2-2.3.4/dist/assets/owl.carousel.min.css');
};
add_action('wp_enqueue_scripts','add_vendor');


//ADD PRINCIPAIS ARQUIVOS
function add_assets(){
    // css
    wp_enqueue_style( 'style-main', ROOT . '/style.css' );
    wp_enqueue_style( 'style-global', ROOT . '/assets/css/global.css' );
    wp_enqueue_style( 'style-responsivo', ROOT . '/assets/css/responsivo.css' );
    // js
    wp_enqueue_script( 'js-main', ROOT . '/assets/js/main.js', array(), false, true);
    //font
    wp_enqueue_style( 'Poppins', ROOT . "/assets/font/");
}
add_action('wp_enqueue_scripts','add_assets');

//ADD MENU
function add_menu(){
    register_nav_menus(
        ["menu-principal"=>__('Menu Principal')]
    );
}
add_action('init','add_menu');

//GET POST E PAGINACAO
function pagina_atual(){
    $pagina = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    return $pagina;
}

function query_postagens( $args = []){
    $args_post = array_merge($args, ['post_type'=>'post']);
    $the_query = new WP_Query( $args_post );
    return $the_query;
}

function paginacao($query) {
    $big = 999999999;
    echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $query->max_num_pages,
        'prev_next'=>true,
        'prev_text' => __ ('<span><<<<<</span>'),
	    'next_text' => __ ('<span>>>>>></span>'),
        'type'=>'list'
    ) );
}
